package com.jirat.emicalculate

import android.annotation.SuppressLint
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.jirat.emicalculate.databinding.ActivityMainBinding
import java.text.DecimalFormat

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.bmiButton.setOnClickListener { calculateEMI() }
        binding.costOfWeightEditText.setOnKeyListener{ view, keyCode, _ -> handleKeyEvent(view, keyCode)}
        binding.costOfHeightEditText.setOnKeyListener{ view, keyCode, _ -> handleKeyEvent(view, keyCode)}

    }

    @SuppressLint("SetTextI18n")
    private fun calculateEMI() {
        val stringHeight = binding.costOfHeightEditText.text.toString()
        val stringWeight = binding.costOfWeightEditText.text.toString()
        val costHeight = stringHeight.toDoubleOrNull()
        val costWeight = stringWeight.toDoubleOrNull()

        if (costHeight == 0.0 || costHeight == null || costWeight == 0.0 || costWeight == null) {
            binding.bmiScore.text = getString(R.string.bmi_0_0, "0.0")
            return
        }

        val calBMI = costWeight / ((costHeight / 100) * (costHeight / 100))
        val df = DecimalFormat("#.#")
        val formatBMI = df.format(calBMI)
        binding.bmiScore.text = getString(R.string.bmi_0_0, formatBMI)

        when (formatBMI.toDouble()) {
            in 0.0..18.5 -> "UNDERWEIGHT".also { binding.bmiResult.text = it }
            in 18.5..24.9 -> "NORMAL".also { binding.bmiResult.text = it }
            in 25.0..29.9 -> "OVERWEIGHT".also { binding.bmiResult.text = it }
            else -> "OBESE".also { binding.bmiResult.text = it }
        }
    }

    private fun handleKeyEvent(view: View, keyCode: Int): Boolean {
        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            // Hide the keyboard
            val inputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
            return true
        }
        return false
    }
}